import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Contato } from './contato/contato.model';

export class ImMemoryDataService implements InMemoryDbService{

    createDb():{} {
        let contatos: Contato[] = [
            {id:1, nome:'Wagner Lima',email:'wagner@lima.com', telefone:'81 991858381'},
            {id:2, nome:'Allan Lima',email:'allan@lima.com', telefone:'81 981198009'},
            {id:3, nome:'Lara Lima',email:'lara@lima.com', telefone:'81 996967596'},
            {id:4, nome:'Tamires Lima',email:'tamires@lima.com', telefone:'81 994947495'}
        ];
        return { contatos }
    }
}