import { Component, OnInit } from "@angular/core";
import { ContatoService } from "./contato.service";
import { ActivatedRoute, Params } from "@angular/router";
import { Location } from "@angular/common";
import { Contato } from "./contato.model";

@Component({
    moduleId:module.id,
    selector: 'contato-detalhe',
    templateUrl: 'contato-detalhe.component.html',
})
export class ContatoDetalheComponent implements OnInit{

    contato:Contato;

    private isNew:boolean = true;

    constructor(
        private contatoService: ContatoService,
        private router: ActivatedRoute,
        private location: Location
    ){}

    ngOnInit():void{
        this.contato = this.contatoService.getDefaultContact();
        this.isNew = true;
        this.router.params.forEach((param:Params)=>{
            if(+param['id']){
                this.isNew = false;
                this.contatoService.getContato(+param['id'])
                    .then((contato)=>this.contato=contato);
                }
            }                
        )
    }

    onSubmit():void{
        
        console.log('teste');

        let promise;
        if(this.isNew)
            promise = this.contatoService.create(this.contato);
        else
            promise = this.contatoService.update(this.contato);

        promise.then(url => this.location.back());
    }

    getFormGroupClass(isValid: boolean, isPristine:boolean){
        return {'form-group':true,
                'has-danger':!isValid && !isPristine,
                'has-success': isValid && !isPristine
                }
    }

    getFormControlClass(isValid: boolean, isPristine:boolean){
        return {'form-control':true,
                'form-control-danger':!isValid && !isPristine,
                'form-control-success': isValid && !isPristine
                }
    }

    goBack():void{
        this.location.back();
    }
    
}