import { Injectable } from "@angular/core";
import { CONTATOS } from "./contatos.mock";
import { Contato } from "./contato.model";
import { Http, Headers, Response } from "@angular/http";
import "rxjs/add/operator/toPromise";
import { Observable } from "rxjs";

@Injectable()
export class ContatoService{

    private contatosUrl:string = 'app/contatos';
    private headers:Headers = new Headers({'Content-Type':'application/json'});

    constructor(
        private http:Http
    ){}

    contatos: Contato[] = CONTATOS;

    getContatos():Promise<Contato[]>{
        return this.http.get(this.contatosUrl)
            .toPromise()
            .then(response => response.json().data as Contato[])
            .catch(this.handlerError);
    }

    getContato(id:number):Promise<Contato>{
        return this.getContatos()
        .then((contatos: Contato[])=>contatos.find(contato => contato.id === id));
    }

    getDefaultContact():Contato{
        return new Contato(0,'','','');
    }

    handlerError(err:any):Promise<any>{
        console.log("Error", err);
        return Promise.reject(err.message || err);
    }

    create(contato:Contato):Promise<Contato>{
        console.log('insert');
        return this.http
        .post(this.contatosUrl,JSON.stringify(contato),{headers:this.headers})
        .toPromise()
        .then((response:Response) => {
            return response.json().data as Contato;
        }).catch(this.handlerError);
    }

    update(contato:Contato):Promise<Contato>{

        const url = `${this.contatosUrl}/${contato.id}`;

        console.log(contato.id);

        return this.http
        .put(url,JSON.stringify(contato),{headers:this.headers})
        .toPromise()
        .then(() => contato as Contato)
        .catch(this.handlerError);
    }

    delete(contato:Contato):Promise<Contato>{

        const url = `${this.contatosUrl}/${contato.id}`;
        return this.http
        .delete(url,{headers:this.headers})
        .toPromise()
        .then(() => contato as Contato)
        .catch(this.handlerError);
    }
    search(term:string):Observable<Contato[]>{
        return this.http
        .get(`${this.contatosUrl}/?nome=${term}`)
        .map((res:Response)=> res.json().data as Contato[]);
    }
}