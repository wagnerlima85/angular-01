"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const contato_service_1 = require("./contato.service");
const dialog_services_1 = require("../dialog.services");
let ContatoListaComponent = class ContatoListaComponent {
    constructor(contatoService, dialogService) {
        this.contatoService = contatoService;
        this.dialogService = dialogService;
        this.contatos = [];
    }
    ngOnInit() {
        this.contatoService.getContatos().then((contatos) => {
            this.contatos = contatos;
        }).catch(err => console.log(err));
    }
    onDelete(c) {
        this.dialogService.confirm('Deseja excluir o contato ' + c.nome)
            .then((canDelete) => {
            if (canDelete) {
                this.contatoService.delete(c)
                    .then(() => {
                    this.contatos = this.contatos.filter(ct => ct.id != c.id);
                    this.showMessages({
                        type: 'success',
                        message: 'Contato excluído com sucesso'
                    });
                }).catch(err => {
                    this.showMessages({
                        type: 'danger',
                        message: 'Não foi possível excluir contato'
                    });
                    console.log(err);
                });
            }
        });
        console.log(c);
    }
    showMessages(msg) {
        this.mensagem = msg;
        this.montarClass(msg.type);
        if (this.currentTimeout) {
            clearTimeout(this.currentTimeout);
        }
        this.currentTimeout = setTimeout(() => {
            this.mensagem = undefined;
        }, 3000);
    }
    montarClass(type) {
        this.classesCSS = {
            'alert': true
        };
        this.classesCSS['alert-' + type] = true;
    }
};
ContatoListaComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'contato-lista',
        templateUrl: 'contato-lista.component.html'
    }),
    __metadata("design:paramtypes", [contato_service_1.ContatoService,
        dialog_services_1.DialogService])
], ContatoListaComponent);
exports.ContatoListaComponent = ContatoListaComponent;
//# sourceMappingURL=contato-lista.component.js.map