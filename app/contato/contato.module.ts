import { NgModule } from '@angular/core';
import { ContatoListaComponent } from './contato-lista.component';
import { CommonModule } from '@angular/common';
import { ContatoRoutingModel } from './contato-routing.model';
import { ContatoDetalheComponent } from './contato-detalhe.component';
import { ContatoService } from './contato.service';
import { FormsModule } from "@angular/forms";
import { ContatoBuscaComponent } from './contato-busca.component';

@NgModule({
    imports:[
        CommonModule,
        ContatoRoutingModel,
        FormsModule
    ],
    declarations:[
        ContatoListaComponent,
        ContatoDetalheComponent,
        ContatoBuscaComponent
    ],
    exports:[
        ContatoListaComponent,
        ContatoBuscaComponent
    ],
    providers:[
        ContatoService
    ]
})
export class ContatoModule{

}