import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Contato } from './contato.model';
import { Subject } from 'rxjs/Subject';
import { ContatoService } from './contato.service';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'contato-busca',
    templateUrl: 'contato-busca.component.html',
    styles:[`.list-group-item{
        cursor:pointer;
    }`]
})

export class ContatoBuscaComponent implements OnInit {

    contatos:Observable<Contato[]>;
    private termSearch:Subject<string> = new Subject<string>();

    constructor(
        private contatoService: ContatoService,
        private router: Router
    ){}

    ngOnInit():void {
        this.contatos = this.termSearch
        .debounceTime(1000) //Setando timeout do evento das teclas
        .distinctUntilChanged() //Ignore se o termo pesquisado for igual ao anterior
        .switchMap(t => t ? this.contatoService.search(t) : Observable.of<Contato[]>([]))
        .catch(err => {
            console.log(err);
            return Observable.of<Contato[]>([]);
        });

     }

    searchName(term:string):void{
        this.termSearch.next(term);
    }

    goToContactDetail(c:Contato):void{
        let link = ['contato/save', c.id];
        this.router.navigate(link);
    }

}