import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContatoListaComponent } from "./contato-lista.component";
import { ContatoDetalheComponent } from "./contato-detalhe.component";

const contatosRouting:Routes = [
{
    path:'contato',
    component:ContatoListaComponent,
},
{
    path:'contato/save',
    component:ContatoDetalheComponent
},
{
    path:'contato/save/:id',
    component:ContatoDetalheComponent
}
];

@NgModule({
    imports:[RouterModule.forChild(contatosRouting)],
    exports:[RouterModule]
})
export class ContatoRoutingModel{
    
}