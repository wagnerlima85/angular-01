import { Component, OnInit } from '@angular/core';
import { Contato } from './contato.model';
import { ContatoService } from './contato.service';
import { DialogService } from '../dialog.services';

@Component({
    moduleId:module.id,
    selector:'contato-lista',
    templateUrl:'contato-lista.component.html'
})
export class ContatoListaComponent implements OnInit{
    contatos: Contato[] = [];
    mensagem:{};
    classesCSS:{};
    private currentTimeout:any;

    constructor(
        private contatoService:ContatoService,
        private dialogService:DialogService
    ){}

    ngOnInit():void{
        this.contatoService.getContatos().then((contatos:Contato[])=>{
            this.contatos = contatos;
        }).catch(err=> console.log(err));
    }
    onDelete(c:Contato):void{
        this.dialogService.confirm('Deseja excluir o contato '+c.nome)
            .then((canDelete:boolean) => {
                if(canDelete){
                    this.contatoService.delete(c)
                    .then(()=>{
                        this.contatos = this.contatos.filter(ct => ct.id != c.id);
                        this.showMessages({
                            type:'success',
                            message:'Contato excluído com sucesso'
                        });
                    }).catch(err =>{
                        this.showMessages({
                            type:'danger',
                            message:'Não foi possível excluir contato'
                        });
                        console.log(err);
                    })
                }
            })
        console.log(c);
    }

    private showMessages(msg: {type:string, message:string}):void{
        this.mensagem = msg;
        this.montarClass(msg.type);

        if(this.currentTimeout){
            clearTimeout(this.currentTimeout);
        }

        this.currentTimeout=setTimeout(()=>{
            this.mensagem = undefined;
        },3000)
    }
    private montarClass(type:string):void{
        this.classesCSS = {
            'alert':true
        }
        this.classesCSS['alert-'+type] =true;
    }
}