import './util/rxjs-extensions';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { ImMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { ContatoModule } from './contato/contato.module';
import { AppRoutingModel } from './app-routing.model';
import { RouterModule } from '@angular/router';
import { DialogService } from './dialog.services';

@NgModule({
    imports: [
        BrowserModule,
        ContatoModule,
        AppRoutingModel,
        HttpModule,
        InMemoryWebApiModule.forRoot(ImMemoryDataService)
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        DialogService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}